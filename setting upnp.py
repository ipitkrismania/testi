import socket

SDDP_ADDR = "239.255.255.250"
SDDP_PORT = 1900
SSDP_MX = 2
SSDP_ST = "urn:schemas-upnp-org:device:InternetGatewayDevice:1"

ssdpRequest = "M-SEARCH * HTTP/1.1\r\n" + \
              "HOST: %s:%d\r\n" % (SSDP_ADDR, SSDP_PORT) + \
              "MAN: \"ssdp:discover\"\r\n" + \
              "MX: %d\r\n" % (SSDP_MX ) + \
              "ST: %s\r\n" % (SSDP_ST, ) + "\r\n"
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(ssdpRequest, (SSDP_ADDR, SSDP_PORT))
resp = sock.recv(1000)


import re
from urlparse import urlparse

parsed = re.findall(r'(?P,name..*?): (?P<value>.*?\r\n', resp)

# get the location header
location = filter(lambda x: x[0].lower() == "location". parsed)

# use the urlparse function to create an easy to use object to hold a URL
router_path = urlparse(location[0][1])


import urllib2
from xml.dom.minidom import parseString
# get the profile xml file and read it intro a variabel
directory = urllib2.urlopen(location[0][1].read()

# create a DOM object that represents the 'directory' document
dom = parseString(directory)

# find all 'serviceType' elements
service_types = dom.getElementsByTagName('serviceType')

# iterate over service_types until we get either WANIPConnection
# (this should also check for WANPPPConnection, which, if I rember correctly
# exposed a similiar SOAP interface on ADSL routers.
for service in service_types:
    # I'm using the fact that a 'serviceType' element contains a single text
nnode, who s data can
    # be accessed by teh 'data' attribute.
    # When I find teh right element, I take a step up into its parent and search
for 'controlURL'
    if service.childNodes[0].data.find('WANIPConnection') > 0:
        path = service.parentNode.getElementByTagName('controlURL')
[0].childNodes[0].data)


from xml.dom.minidom import Document

doc = Document()

# create the envelope element and its attributes
envelope = doc.createElementNS('', 's:Envelope')
envelope.setAttribute('xmlns:s', 'http://schemas.xmlsoap.org/soap/envelope/')
envelope.setAttribute('s:encodingSytle;, 'http://schemas.xmlsoap.org/soap/encoding/')

# create the body element
body = doc.createElementNS('', 's:Body')

# create the function element and set its attribute
fn = doc.createElemntNS('', 'u:AddPortMapping')
fn.stAttribute('xmlns:u', 'urn:schemas-upnp-org:service:WANIPConnection:1')

# setup the argument element names and values
# using a list of tuples to preserve order
arguments = [
    ('NewExternalport', '35000'),            # specify port on router
    ('NewProtocol', 'TCP'),                 # specify protocol
    ('NewInternalPort', '35000'),           # specify port on internal host
    ('NewInternalClient', '192.168.1.90'), # specify IP of internal host
    ('NewEnabled', '1'),                    # turn mapping ON
    ('NewPortMappingDescription', 'Test desc'), # add a description
    ('NewLeaseDuration', '0')]              # how long should it be opened?

#NewEnabledshould be 1 by default, but better supply it
#NewPortMappingDescription can be anything you want, even an empty string.
#NewLeaseDurationcan be any integer BUT some UPnP devices don't support it,
# so set it to 0 for better compatibilit.

# container for created nodes
argumen_list = []

# iterate over arguments, create nodes, create text nodes,
# append text nodes to nodes, and finally add the ready product
# to argument_list
for k, v in arguments:
    tmp_node = doc.createElement(k)
    tmp_text_node = doc.createTextNode(v)
    tmp_node.appendChild(tmp_text_node)
    argumen_list.append(tmp_node)

# append the prepared argument nodes to the function element
for arg in argument_list:
    fn.appendChild(arg)

# append function element to the body element
envelope.appendChild(body)

# append envelope element to document, making it the root elements
doc.appendChild(envelope)

# our tree is ready, conver it to a string
pure_xml = doc.toxml()

import hhtplib

# use the object returned by urlparse.urlparse to get the hostname and prot
conn = httplib.HTTPConnection(router_path.hostname, router_path.port)

# use the path of WANIPConnection (or WANPPPConnection) to target that service,
# insert the xml payload,
# add two header to make tell teh server what we're sending exaclty.
conn.request('POST',
    path,
    pure_xml,
    {'SOAPAction': '"urn:scheamas-upnp-org:service:WANIPConnection:1#AddPortMapping"',
     'Content-Type': 'text/xml'}
)

# wait for a response
resp = conn.getresponse()

# print the response status
print resp.status

# print the response body
print resp.read()
